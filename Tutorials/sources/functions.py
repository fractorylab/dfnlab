import os 

def isOnTestServer() :
    import os.path
    from os import path
    return path.exists('D:/Shares/Users/Fractory/GitLab-Runner/python38/python.exe')

def generateRandomFilename():
    import os 
    path = os.getcwd()
    pathFolder = os.getcwd()+"/Tmp"
    if not os.path.isdir(pathFolder) :
        os.mkdir(pathFolder)   
    import random
    import string
    file = ''.join(random.choice(string.ascii_lowercase) for i in range(8))
    filename = pathFolder + '/'  + ''.join(random.choice(string.ascii_lowercase) for i in range(8))
    return filename

def plot_domain(domain,value=None,interactive=False,save=False):
    if isOnTestServer() : return    
    import pyvista as pv
    import dfnlab.IO as io
    import dfnlab.Domains as dom
    import dfnlab.System as sys

    import numpy as np
    
    filename = generateRandomFilename()  
    io_obj = io.DFNIO_Object(domain)
    io_obj.createVTKDataDomain()
    print(filename)
    io_obj.writeVTKOnDomain(filename+'.vtp')


    visu = pv.read(filename+'.vtp')
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.set_background('white')
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    if value==None:
        p.add_mesh(visu, style='wireframe', color='black')
    else:
        p.add_mesh(visu, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)  
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+'.vtp')
    

def plot_domain_and_geometries(system,interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as io
    import pyvista as pv
    import numpy as np
    filename = generateRandomFilename()  
    io_obj = io.DFNIO_Object(system)
    io_obj.createVTKDataDomain()
    io_obj.createVTKDataGeometries()
    io_obj.writeVTKOnDomain(filename+'_domain')
    io_obj.writeVTKOnGeometries(filename+"_geometries")
    visu1 = pv.read(filename+"_domain.vtp")
    visu2 = pv.read(filename+"_geometries.vtp")
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.set_background('white')
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)            
    p.add_mesh(visu1, style='wireframe', color='black')
    p.add_mesh(visu2, color='grey', show_edges=False, lighting=True)
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+"_domain.vtp")
        os.remove(filename+"_geometries.vtp")
    
def plot_dfn(system,value=None,log=False,interactive=False,save=False):
    if isOnTestServer() : return    
    import pyvista as pv
    import dfnlab.IO as io

    import numpy as np
    filename = generateRandomFilename()  
    io_obj = io.DFNIO_Object(system)
    io_obj.createVTKDataDomain()
    io_obj.createVTKDataDFN()
    if value!=None:
        io_obj.appendDataOnDFN("clusterId")
    io_obj.writeVTKOnDomain(filename+'_domain.vtp')
    io_obj.writeVTKOnDFN(filename+"_dfn.vtp")




    visu1 = pv.read(filename+"_domain.vtp")
    visu2 = pv.read(filename+"_dfn.vtp")
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    p.set_background('white')
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12) 
    p.add_mesh(visu1, style='wireframe', color='black')
    if value==None:
        p.add_mesh(visu2, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
    else:
        if log==False:
            p.add_mesh(visu2, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
        else:
            visu2['log('+value+')'] = np.log(visu2[value])
            p.add_mesh(visu2, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+"_domain.vtp")
        os.remove(filename+"_dfn.vtp")
    
    
def plot_dfn_and_slice(system,value=None,log=False,axis='z',interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as io
    import pyvista as pv
    import numpy as np
    filename = generateRandomFilename()  
    io_obj = io.DFNIO_Object(system)
    io_obj.createVTKDataDomain()
    io_obj.createVTKDataDFN()
    if value!=None:
        io_obj.appendDataOnDFN("clusterId")
    io_obj.writeVTKOnDomain(filename+'_domain')
    io_obj.writeVTKOnDFN(filename+"_dfn")
    visu1 = pv.read(filename+"_domain.vtp")
    visu2 = pv.read(filename+"_dfn.vtp")

    p = pv.Plotter(shape=(1,2), notebook=True,window_size=(1600,640))
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    p.set_background('white')
    p.subplot(0, 0)
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    p.add_text("DFN", color='black')
    p.add_mesh(visu1, style='wireframe', color='black')
    if value==None:
        p.add_mesh(visu2, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
    else:
        if log==False:
            p.add_mesh(visu2, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
        else:
            visu2['log('+value+')'] = np.log(visu2[value])
            p.add_mesh(visu2, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)

    p.subplot(0, 1)
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    p.add_text("virtual outcrop", color='black')
    if axis=='z':
        direction = normal=[0, 0, 1]
    elif axis=='y':
        direction = normal=[0, 1, 0]
    elif axis=='x':
        direction = normal=[1, 0, 0]
    single_slice = visu2.slice(normal=direction)
    p.add_mesh(single_slice, color='black')
    p.view_vector(direction)
    p.view_xy()
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+"_domain.vtp")
        os.remove(filename+"_dfn.vtp")
    
def plot_dfn_and_geometries(system,value=None,log=False,interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as io
    import pyvista as pv
    import numpy as np
    filename = generateRandomFilename()  
    io_obj = io.DFNIO_Object(system)
    io_obj.createVTKDataDomain()
    io_obj.createVTKDataGeometries()
    io_obj.createVTKDataDFN()
    if value!=None:
        io_obj.appendDataOnDFN("clusterId")
    io_obj.writeVTKOnDomain(filename+'_domain')
    io_obj.writeVTKOnGeometries(filename+"_geometries")
    io_obj.writeVTKOnDFN(filename+"_dfn")
    visu1 = pv.read(filename+"_domain.vtp")
    visu2 = pv.read(filename+"_dfn.vtp")
    visu3 = pv.read(filename+"_geometries.vtp")
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.set_background('white')
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12) 
    p.add_mesh(visu1, style='wireframe', color='black')
    if value==None:
        p.add_mesh(visu2, color='white', show_edges=True, edge_color='black', lighting=True, show_scalar_bar=True)
    else:
        if log==False:
            p.add_mesh(visu2, scalars=value, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
        else:
            visu2['log('+value+')'] = np.log(visu2[value])
            p.add_mesh(visu2, scalars='log('+value+')', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
    p.add_mesh(visu3, color='grey', show_edges=False, lighting=True)
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+"_domain.vtp")
        os.remove(filename+"_dfn.vtp")
        os.remove(filename+"_geometries.vtp")

    
def plot_dfn_hydro(system,log=False,interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as io
    import pyvista as pv
    import numpy as np
    filename = generateRandomFilename()  
    io_obj = io.DFNIO_Object(system)
    io_obj.createVTKDataHydro()
    io_obj.createVTKDataDomain()
    io_obj.writeVTKOnDFNHydro(filename+'_hydro')
    visu2 = pv.read(filename+"_hydro.vtp")
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.set_background('white')
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12) 
    if log==False:
        p.add_mesh(visu2, scalars='Transmissivity', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
    else:
        visu2['log(Transmissivity)'] = np.log(visu2['Transmissivity'])
        p.add_mesh(visu2, scalars='log(Transmissivity)', cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+"_hydro.vtp")
    
    
def plot_mesh(mesh,field=None, fieldName = None,interactive=False,save=False, **kwargs):
    if isOnTestServer() : return
    import dfnlab.IO as io
    import pyvista as pv
    import numpy as np

    logscale  = kwargs.get("log_scale",False)
    colormap = kwargs.get("colormap",'coolwarm')

 
    
    filename = generateRandomFilename()
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)

    if(isinstance(field,list) ):
        nx = int(len(field)/2)+1
        ny = int(len(field)%2)+1
        p = pv.Plotter(shape=(nx,ny), notebook=True,window_size=(ny*800,nx*640))
 
         

        for j in range(len(field)):
            p.subplot(int(j/2),j%2)        
            Aw = io.DFNIO_Object(mesh)
            Aw.appendDataOnMesh(field[j],fieldName[j])
            Aw.writeVTKOnMesh(filename,True)

            visu = pv.read(filename+".vtp")
            p.set_background('white')

            if(field[j]  == None or fieldName[j] == None):
                p.add_mesh(visu, show_edges=True, color='white')
            else:
                if(isinstance(logscale,list)):
                    p.add_mesh(visu, scalars=fieldName[j], cmap=colormap,lighting=True, show_scalar_bar=True, scalar_bar_args=argss, log_scale=logscale[j])
                else:
                    p.add_mesh(visu, scalars=fieldName[j], cmap=colormap,lighting=True, show_scalar_bar=True, scalar_bar_args=argss, log_scale=logscale)

            p.show_grid(color='black')
        if interactive:
            p.show(auto_close=False,jupyter_backend="panel")
        else:
            p.show(auto_close=False,jupyter_backend="static")
        if not save:
            os.remove(filename+".vtp")
    else:
        Aw = io.DFNIO_Object(mesh)

        if(not(field  == None or fieldName == None)):
            Aw.appendDataOnMesh(field,fieldName)
        Aw.writeVTKOnMesh(filename,True)
        visu = pv.read(filename+".vtp")
        p = pv.Plotter(notebook=True,window_size=(800,640))
        p.set_background('white')
        if(field  == None or fieldName == None):
            p.add_mesh(visu, show_edges=True, color='white')
        else:
            p.add_mesh(visu, scalars=fieldName, cmap=colormap,lighting=True, show_scalar_bar=True, scalar_bar_args=argss, log_scale=logscale)

        p.show_grid(color='black')
        if interactive:
            p.show(auto_close=False,jupyter_backend="panel")
        else:
            p.show(auto_close=False,jupyter_backend="static")
        if not save:
            os.remove(filename+".vtp")
    
    
def plot_stereonet(fnet):
    if isOnTestServer() : return
    import mplstereonet
    import pylab as pl
    import matplotlib.pyplot as pyplot
    pyplot.rcParams['figure.dpi'] = 150
    strike = []
    dip =[]
    for frac in fnet:
        strike.append(frac.getDipDirection()-90.)
        dip.append(frac.getDip())
    fig = pyplot.figure()
    ax1 = fig.add_subplot(121, projection='stereonet')
    pos = ax1.density_contourf(strike, dip, measurement='poles', method='schmidt',gridsize=50,extend='neither',cmap='coolwarm')
    ax1.set_title('Contour density')
    ax1.set_azimuth_ticks([])
    fig.colorbar(pos, ax=ax1)
    ax1.grid(kind='polar')
    ax2 = fig.add_subplot(122, projection='stereonet')
    ax2.pole(strike, dip, markersize=0.1)
    ax2.set_title('Fracture poles')
    ax2.grid(kind='polar')
    pyplot.show()


def plot_size_distribution(system,nbin=20):
    if isOnTestServer() : return
    import matplotlib.pyplot as pyplot
    import dfnlab.Analysis as an
    pyplot.rcParams['figure.dpi'] = 100
    csfont = {'fontname':'Times New Roman', 'fontsize':14}
    pyplot.rcParams['mathtext.fontset'] = 'dejavuserif'
    fig = pyplot.figure()
    ax = pyplot.axes()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel('Fracture size $l$',**csfont)
    ax.set_ylabel('Fracture size distribution $n(l)$',**csfont)
    analyser = an.Analyser(system)
    size_distribution=analyser.fractureSizeDistribution(nbin)
    lmean = size_distribution["size"]
    nl =size_distribution['density_distribution']
    ax.plot(lmean,nl,marker='o',markersize=5,linestyle='-',color='blue')
    pyplot.title("Fracture size distribution")
    pyplot.show()
    
def plot_size_distribution_ufm(system,nbin,nrate,C,a,alphaU):
    if isOnTestServer() : return
    import matplotlib.pyplot as pyplot
    import dfnlab.Analysis as an
    pyplot.rcParams['figure.dpi'] = 100
    csfont = {'fontname':'Times New Roman', 'fontsize':14}
    pyplot.rcParams['mathtext.fontset'] = 'dejavuserif'
    fig = pyplot.figure()
    ax = pyplot.axes()
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.set_xlabel('Fracture size $l$',**csfont)
    ax.set_ylabel('Fracture size distribution $n(l)$',**csfont)
    analyser = an.Analyser(system)
    size_distribution=analyser.fractureSizeDistribution(nbin)
    lmean = size_distribution["size"]
    nl =size_distribution['density_distribution']
    ax.plot(lmean,nl,marker='o',markersize=5,linestyle='-',label = 'UFM',color='blue')
    dilute = [nrate/C*pow(l,-a) for l in lmean]
    ax.plot(lmean,dilute,marker='o',markersize=0,linestyle='--',label = 'dilute',color='green')
    dense = [2.5*pow(l,-4) for l in lmean]
    ax.plot(lmean,dense,marker='o',markersize=0,linestyle='--',label = 'dense',color='red')

def plot_correlation_pair_function(system,D,rmin=1,nbin=20):
    if isOnTestServer() : return
    import matplotlib.pyplot as pyplot
    import dfnlab.Analysis as dfnA
    import numpy as np
    analyser = dfnA.Analyser(system)
    c2r = analyser.correlationPairFunction3D(rmin,nbin)
    # prepare plot
    csfont = {'fontname':'Times New Roman', 'fontsize':14}
    pyplot.rcParams['mathtext.fontset'] = 'dejavuserif'
    pyplot.rcParams['figure.dpi'] = 100
    fig = pyplot.figure() #(figsize=(8, 7))
    ax = pyplot.axes()
    ax.set_xscale("log")
    ax.set_yscale("log")
    # get
    r = np.array(c2r['r_mean'])
    idx1 = [True if r[i]!=0 else False for i in range(0,len(r)) ]
    Dc = np.array(c2r['correlation_dimension'])
    idx2 = [True if Dc[i]!=0 else False for i in range(0,len(Dc)) ]
    idx = idx1 and idx2
    r = r[idx]
    Dc = Dc[idx]
    ax.plot(r,Dc,marker='o',markersize=5, linewidth=2, color='k',label='DFN')
    ax.set_ylim(2,3.5)
    ax.axhline(D,linestyle = 'dashed', color = 'grey', label='$D='+str(D)+'$')
    pyplot.xlabel('Pair distance $r$',**csfont)
    pyplot.ylabel('Correlation dimension $D$',**csfont)
    pyplot.title("Correlation dimension")
    pyplot.legend()
    pyplot.show()
    
def plot_octree_and_domains(domain,domainList,interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np
    import random
    filename = generateRandomFilename()  
    writer = dfn_io.DFNIO_Object(domain)
    writer.createVTKDataOctree()
    writer.writeVTKOnOctree(filename)
    p = pv.Plotter(shape=(1,2), notebook=True,window_size=(1600,640))
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    p.set_background('white')
    p.subplot(0, 0)
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    p.add_text("Domains", color='black')
    for dom in domainList:
        mesh_dom = pv.read(dom)
        p.add_mesh(mesh_dom, color=(random.random(),random.random(),random.random())) #, edge_color='black',show_edges=True)
    p.subplot(0, 1)
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    p.add_text("Octree", color='black')
    mesh_octree = pv.read(filename+".vtu")
    p.add_mesh(mesh_octree, scalars='depth', edge_color='black', cmap='coolwarm', show_edges=True,lighting=False, opacity = 1, show_scalar_bar= False)
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+".vtu")
    
def plot_octree(domain,interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np
    import random
    filename = generateRandomFilename()  
    writer = dfn_io.DFNIO_Object(domain)
    writer.createVTKDataOctree()
    writer.writeVTKOnOctree(filename)
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    p.set_background('white')
    mesh_octree = pv.read(filename+".vtu")
    thresholed = mesh_octree.threshold(scalars= "octree area number", value = 0)
    p.add_mesh(thresholed, scalars='octree area number', edge_color='black', cmap='coolwarm', show_edges=True,lighting=False, opacity = 1, show_scalar_bar= False)
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+".vtu")

def plot_graph(graph,graphFlow=None,interactive=False,save=False):
    if isOnTestServer() : return
    import os
    import dfnlab.IO as io
    import pyvista as pv    

    filename = generateRandomFilename()  
    writer = io.DFNIO_Object(graph, create = False)
    writer.createVTKDataGraph(graphFlow)
    writer.writeVTKOnGraph(filename)
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.set_background('white')
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    visu_graph = pv.read(filename+".vtp")
    visu_graph2 = pv.read(filename+".vtp")
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)
    if graphFlow!=None:
        p.add_mesh(visu_graph, scalars="nodePressure", line_width=2,cmap="coolwarm",show_scalar_bar=True, log_scale=False,scalar_bar_args=argss)
    else:
        p.add_mesh(visu_graph, color='black',line_width=2)
    p.add_mesh(visu_graph2, style="points",show_scalar_bar=False,color="black")
    p.show_grid(color='black')
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(filename+".vtp")

def plot_flow_particles(mesh, particles,field=None, fieldName = None,interactive=False,save=False):
    if isOnTestServer() : return
    import dfnlab.IO as dfn_io
    import pyvista as pv
    import numpy as np
    
    filenameFlow = generateRandomFilename()
    fileNameParticles = generateRandomFilename()
    
    Aw = dfn_io.DFNIO_Object(mesh)
    argss = dict(color='black', n_labels = 3, title_font_size = 14, label_font_size=12)

    if(not(field  == None or fieldName == None)):
        Aw.appendDataOnMesh(field,fieldName)

    Aw.writeVTKOnMesh(filenameFlow,True)
    dfn_io.write_particle_file( particles,fileNameParticles+".vtp")

    visu = pv.read(filenameFlow+".vtp")
    p = pv.Plotter(notebook=True,window_size=(800,640))
    p.set_background('white')
    p.add_axes(xlabel="East",ylabel="North",zlabel="Up", color='black')
    p.show_axes()
    if(field  == None or fieldName == None):
        p.add_mesh(visu, show_edges=True, color='white')
    else:
        p.add_mesh(visu, scalars=fieldName, cmap='coolwarm',lighting=True, show_scalar_bar=True, scalar_bar_args=argss)

    visu = pv.read(fileNameParticles+".vtp")
 
    p.add_mesh(visu, color = 'black',line_width = 2)
    p.show_grid(color='black')
    if interactive:
        p.show(auto_close=False,jupyter_backend="panel")
    else:
        p.show(auto_close=False,jupyter_backend="static")
    if not save:
        os.remove(fileNameParticles+".vtp")
        os.remove(filenameFlow+".vtp")

