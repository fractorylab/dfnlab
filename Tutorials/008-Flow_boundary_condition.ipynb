{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "ev39buVi6c8k6SuQDDIg",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    },
    "tags": []
   },
   "source": [
    "# __Assigning Hydraulic Boundary Conditions in DFN.lab__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "SIXhhhKnJiGsj0cYxVzM",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "### __Problem Statement__ \n",
    "\n",
    "In this tutorial, we will learn how to assign hydraulic boundary conditions to a Discrete Fracture Network (DFN) system using DFN.Lab. Boundary conditions are crucial for flow simulations, as they determine how fluids enter and exit the system.\n",
    "\n",
    "<center>\n",
    "<div>\n",
    "<img src=\"https://gitlab.com/fractorylab/dfnlab/-/raw/master/Tutorials/img/boundary_conditions.png\" width=\"1000\"/>\n",
    "</div>\n",
    "<br>\n",
    "The image above illustrates the application of boundary conditions on different objects within a DFN system.\n",
    "</center>\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "NWFEHWzIpQhWhizbvPu4",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "### __Theoretical Background__ \n",
    "\n",
    "In DFN.Lab, two primary types of boundary conditions can be applied to model fluid flow through fractured networks:\n",
    "\n",
    "* **Dirichlet Boundary Condition (Fixed Hydraulic Head)**: This condition specifies a constant hydraulic head at a boundary, representing a known fluid pressure. It is often used when you know the exact pressure at an inlet or outlet.\n",
    "  \n",
    "* **Neumann Boundary Condition (Fixed Flux)**: This condition sets a fixed flux or flow rate across a boundary. It defines the derivative of the hydraulic head and controls the flow rate into or out of the system.\n",
    "\n",
    "These conditions allow you to control how fluids enter and exit the system.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "emhJASR9VW7PnymYa3IS",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "### __Learning Objectives__\n",
    "\n",
    "In this tutorial, you will learn how to assign the following boundary conditions in DFN.Lab:\n",
    "\n",
    "- **Permeameter boundary conditions**: Apply fixed hydraulic heads on two opposite boundaries and no-flow conditions on others. This is often used to simulate fluid movement through a system.\n",
    "  \n",
    "- **Imposed heads on boreholes**: Assign specific hydraulic heads to wells or boreholes, representing the pressure at those locations.\n",
    "\n",
    "- **Fixed conditions on fracture intersections**: Set boundary conditions directly on fracture intersections to simulate pressure or flow conditions at points where fractures meet.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "3db8l58hrtMLbkDasX6r",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "### __DFN.Lab Model__\n",
    "\n",
    "To assign hydraulic boundary conditions in DFN.Lab, we will use the `BoundaryCondition` module. This module provides tools to set fixed hydraulic heads or fluxes on various objects in the DFN, such as domains, geometries (e.g., wells or tunnels), and fracture intersections.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "FO5w4TQiKStHIw5Xl6RM",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "import dfnlab.FlowBoundaryConditions as bc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "fVhEpyM0Tk8I7DbReyqG",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "kbcVhvdjDtZnZT6TGK0l",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "## __Example 1: Applying Permeameter Boundary Conditions__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "i1zNhoAwxUyZOCiSdZ4m",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "In this example, we will apply **permeameter boundary conditions** to the domain boundaries. Permeameter conditions are used to simulate flow through the system by fixing hydraulic heads on opposite boundaries (in this case, the $x_{min}$ and $x_{max}$ boundaries) and imposing no-flow conditions on other boundaries.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "MQUgS6gsJjbsRZ6LfzRq",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "First, we initialize a system that includes a cubic domain and a Discrete Fracture Network (DFN) imported from the file `\"Ktest.disk\"`. This sets up the model environment where we will apply our boundary conditions and simulate fluid flow.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "DDdD6FoT7ujbaoGjI917",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "import dfnlab.System as sys\n",
    "import dfnlab.Domains as dom\n",
    "import dfnlab.Fractures as dfn\n",
    "import numpy as np\n",
    "\n",
    "# Initialize the system object\n",
    "system = sys.System()\n",
    "\n",
    "# Create a cubic domain centered at [0, 0, 0] with side length of 1 unit\n",
    "domain = dom.Domain()\n",
    "system.setDomain(domain)\n",
    "domain.buildParallelepiped(center=[0, 0, 0], L1=1.)  # L1 is the length of one side of the cube\n",
    "\n",
    "# Load the DFN (Discrete Fracture Network) from a disk file and add it to the system\n",
    "fnet = dfn.DFN()\n",
    "system.setDFN(fnet)\n",
    "dfn.load_disk_file(fnet, filename='sources/Ktest.disk')  # Load fractures from Ktest.disk\n",
    "\n",
    "# Build the complete system with the domain and fractures\n",
    "system.build()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "lP4HMpvGfd2ewsnUlrtZ",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 2
     }
    }
   },
   "source": [
    "To apply permeameter conditions, we will set fixed hydraulic heads on two opposite boundaries of the cubic domain. Specifically, we will impose a pressure of \\( P = 10 \\, \\text{m} \\) on the \\( x_{min} \\) boundary and \\( P = 0 \\, \\text{m} \\) on the \\( x_{max} \\) boundary. These boundary conditions simulate a flow from high to low pressure across the domain.\n",
    "\n",
    "The domain has six boundaries, with their respective indices:\n",
    "- 0: $ x_{min} $\n",
    "- 1: $ x_{max} $\n",
    "- 2: $ y_{min} $\n",
    "- 3: $ y_{max} $\n",
    "- 4: $ z_{min} $\n",
    "- 5: $ z_{max} $\n",
    "\n",
    "We will set the desired boundary conditions on the appropriate borders.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "nGp07mkbbmHuTtLyZzQh",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "# Apply a hydraulic head of 10 m on the x_min boundary (border index 0)\n",
    "bc.FlowBoundaryCondition.setHead(obj=domain.getBorder(0), head=10)\n",
    "\n",
    "# Apply a hydraulic head of 0 m on the x_max boundary (border index 1)\n",
    "bc.FlowBoundaryCondition.setHead(obj=domain.getBorder(1), head=0)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "qNckPS9ADRm95uc8xYKd",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "For the remaining boundaries ($y_{min}$, $y_{max}$, $z_{min}$, and $z_{max}$), we will apply a **zero flux** condition, meaning no fluid will flow through these boundaries. Although this is the default condition in DFN.Lab, we explicitly enforce it by removing any previously set boundary conditions.\n",
    "\n",
    "Zero flux conditions create impervious boundaries, where no fluid can pass through.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "3JJe9ICkbzPkJULZyZUF",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "# Remove any previously set boundary conditions on the y_min, y_max, z_min, and z_max borders\n",
    "domain.getBorder(2).eraseBoundaryCondition('flow')  # y_min\n",
    "domain.getBorder(3).eraseBoundaryCondition('flow')  # y_max\n",
    "domain.getBorder(4).eraseBoundaryCondition('flow')  # z_min\n",
    "domain.getBorder(5).eraseBoundaryCondition('flow')  # z_max"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "TD0sum05wmMc8BQu5UhY",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 2
     }
    }
   },
   "source": [
    "DFN.Lab provides a convenient function to apply permeameter boundary conditions. In this function:\n",
    "- The first argument is the domain on which the boundary conditions are applied.\n",
    "- The second and third arguments are the hydraulic heads imposed on the two opposite boundaries (for example, $ P = 10 \\, \\text{m} $ on $ x_{min} $ and $ P = 0 \\, \\text{m} $ on $ x_{max} $).\n",
    "- The fourth argument is the direction of the hydraulic gradient, defined as a vector.\n",
    "\n",
    "This function simplifies the process of assigning permeameter conditions in any desired direction.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "WndaMja6ciJ0eylAwtoR",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "# Apply permeameter conditions on the domain\n",
    "# head1 = 10 m on x_min, head2 = 0 m on x_max, direction is along the x-axis (1, 0, 0)\n",
    "bc.FlowBoundaryCondition.setPermeameter(domain=domain, head1=10, head2=0, direction=np.array([1,0,0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "kC3fQlJTkDvQQFXNwVol",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "<center>\n",
    "<div>\n",
    "<img src=\"https://gitlab.com/fractorylab/dfnlab/-/raw/master/Tutorials/img/bc1.png\" width=\"700\"/>\n",
    "</div>\n",
    "<br>\n",
    "The image above illustrates the hydraulic head difference imposed on the boundaries of the domain, simulating fluid flow across the system.\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "fiTlS0sQBRpxyYgpRvjh",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "You can easily retrieve the boundary conditions imposed on any object in the system. This is particularly useful for verifying that the correct boundary conditions have been applied and to check the type and value of the condition (e.g., Dirichlet or Neumann)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "o0qYAPUEwN7RHge9btNl",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": {
      "block": "rbIpqA5xmObgjcT3dtq9",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Boundary condition on border xmin is of dirichlet type and its value is 10.0\n"
     ]
    }
   ],
   "source": [
    "# Get the boundary object for x_min (border index 0)\n",
    "border = domain.getBorder(0)\n",
    "\n",
    "# Retrieve the current boundary condition for the \"flow\" on this border\n",
    "currentBoundaryCondition = border.getBoundaryCondition(\"flow\")\n",
    "\n",
    "# Print the type and value of the boundary condition applied on the x_min border\n",
    "print(\"Boundary condition on border xmin is of {} type and its value is {}\".format(\n",
    "    currentBoundaryCondition.getBoundaryConditionType(), currentBoundaryCondition.getValue()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "2Hyf7uWLk3yNEINaNwfY",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": {
      "block": "0IRloAzCsp1iermplX7M",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Boundary condition on border zmin is of neuman type and its value is 0.0\n"
     ]
    }
   ],
   "source": [
    "# Retrieve the boundary object for z_min (border index 4)\n",
    "border = domain.getBorder(4)\n",
    "\n",
    "# Get the current boundary condition for the \"flow\" on the z_min border\n",
    "currentBoundaryCondition = border.getBoundaryCondition(\"flow\")\n",
    "\n",
    "# Print the type and value of the boundary condition applied on the z_min border\n",
    "print(\"Boundary condition on border zmin is of {} type and its value is {}\".format(\n",
    "    currentBoundaryCondition.getBoundaryConditionType(), currentBoundaryCondition.getValue()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "yQZQm3Xy3mCqT95vR9YG",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "8PKWKbO3XhoNkRpTbI8A",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "## __Example 2: Applying Boundary Conditions on Wells__\n",
    "\n",
    "In addition to domain boundaries, DFN.Lab allows you to impose hydraulic head boundary conditions on geometries such as wells. Wells are 1D objects that represent boreholes in the system, and they can be used to simulate groundwater extraction or injection.\n",
    "\n",
    "In this example, we will create two wells and attach them to the DFN system. Once added to the system, we can apply different hydraulic head conditions to simulate fluid movement through the wells."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "vDfuD79a07ifQqN3cFSF",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "# Define the start and end positions for the first well\n",
    "positionWell1Begin = [0.25, 0.25, 0.5]  # Start at (x=0.25, y=0.25, z=0.5)\n",
    "positionWell1End = [0.25, 0.25, -0.5]   # End at (x=0.25, y=0.25, z=-0.5)\n",
    "\n",
    "# Create the first well and add it to the system\n",
    "well1 = dom.Well1D(point1=positionWell1Begin, point2=positionWell1End, index=1)\n",
    "system.addGeometry(well1)  # Attach the well to the system for simulation\n",
    "\n",
    "# Define the start and end positions for the second well\n",
    "positionWell2Begin = [-0.25, -0.25, 0.5]  # Start at (x=-0.25, y=-0.25, z=0.5)\n",
    "positionWell2End = [-0.25, -0.25, -0.5]   # End at (x=-0.25, y=-0.25, z=-0.5)\n",
    "\n",
    "# Create the second well and add it to the system\n",
    "well2 = dom.Well1D(point1=positionWell2Begin, point2=positionWell2End, index=2)\n",
    "system.addGeometry(well2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "QRaUjJ4Qp1jc3PGTvq2S",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 2
     }
    }
   },
   "source": [
    "Next, we apply a fixed hydraulic head of \\(10 \\, \\text{m}\\) on the first well and \\(0 \\, \\text{m}\\) on the second well. This setup simulates a **doublet pumping** system, where one well injects fluid (high pressure), and the other well extracts fluid (low pressure). The pressure differential drives fluid flow between the two wells through the surrounding fracture network.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "05sBVdzRCsVWYvc7Zoug",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "# Apply a hydraulic head of 10 m on the first well (simulating injection)\n",
    "bc.FlowBoundaryCondition.setHead(well1, 10)\n",
    "\n",
    "# Apply a hydraulic head of 0 m on the second well (simulating extraction)\n",
    "bc.FlowBoundaryCondition.setHead(well2, 0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "DIYqhx5PNxp5PLF956VI",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "<center>\n",
    "<div>\n",
    "<img src=\"https://gitlab.com/fractorylab/dfnlab/-/raw/master/Tutorials/img/bc2.png\" width=\"700\"/>\n",
    "</div>\n",
    "<br>\n",
    "The image above illustrates the pressure difference applied to the two wells, simulating injection in one and extraction in the other.\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "dvofghZ0lMw6uiPjW4SR",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "## __Example 3: Applying Boundary Conditions to Any Object__\n",
    "\n",
    "DFN.Lab allows boundary conditions to be applied not just on domain boundaries or wells, but on any geometric object such as tunnels, borders, or even intersections between fractures.\n",
    "\n",
    "In this example, we will impose a fixed hydraulic head on an intersection between two fractures. This is useful for simulating fluid flow at fracture intersections.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "iooxa": {
     "id": {
      "block": "MpwBOKLfVkkBfZuDvL0n",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 3
     },
     "outputId": null
    }
   },
   "outputs": [],
   "source": [
    "# Retrieve the intersection between two fractures in the network\n",
    "# Here we are getting the intersection between fracture 0 and fracture 9\n",
    "intersection = system.getIntersectionBetweenFractures(fracture1=fnet.getFractureByIndex(0), fracture2=fnet.getFractureByIndex(9))\n",
    "\n",
    "# Apply a fixed hydraulic head of 10 m to the intersection\n",
    "bc.FlowBoundaryCondition.setHead(obj=intersection, head=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "iooxa": {
     "id": {
      "block": "yAQ2ykQh1QJHMs0Zc40I",
      "project": "w5d86MNWp7ma7pxjulzO",
      "version": 1
     }
    }
   },
   "source": [
    "<center>\n",
    "<div>\n",
    "<img src=\"https://gitlab.com/fractorylab/dfnlab/-/raw/master/Tutorials/img/bc3.png\" width=\"700\"/>\n",
    "</div>\n",
    "<br>\n",
    "The image above shows the boundary condition applied to a fracture intersection. \n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### __Inspecting Attached Objects in the System__\n",
    "\n",
    "At any point, you can retrieve and inspect all objects attached to the system using the `system.getAttachedObjects()` function. This includes wells, fractures, tunnels, and intersections. It is useful for verifying that all objects have been added correctly before running simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Attached object: Domain\n",
      "Attached object: Well1D\n",
      "Attached object: Well1D\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n",
      "Attached object: Intersection\n"
     ]
    }
   ],
   "source": [
    "# Retrieve and display all objects currently attached to the system\n",
    "attached_objects = system.getAttachedObjects()\n",
    "\n",
    "# Print the attached objects\n",
    "for obj in attached_objects:\n",
    "    print(f\"Attached object: {type(obj).__name__}\")"
   ]
  }
 ],
 "metadata": {
  "iooxa": {
   "id": {
    "block": "wbgmphSDksOTpX14mNhT",
    "project": "w5d86MNWp7ma7pxjulzO",
    "version": 3
   }
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
